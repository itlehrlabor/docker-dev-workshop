# Einfache Node App

Dieses Projekt beinhaltet eine einfache Node app, die auf der Console ein paar Log Einträge ausgibt. Der Sinn dieses Projekts ist es, die wichtigsten Aspekte von Dockerfile und docker-compose.yml kennen zu lernen.

Damit dieses Projekt erfolgreich ausgeführt werden kann, müssen in einem Container folgende Befehle ausgeführt werden:

*[ ] Der Container basiert auf dem node Image
*[ ] Der Inhalt vom ./app Ordner muss in den Container kopiert werden (z.B. nach /usr/app).
*[ ] Die Dateien package.json und package-lock.json müssen in den Container kopiert werden.
*[ ] Das Arbeitsverzeichnis muss auf /usr/app gewechselt werden (damit alle weiteren Befehle in diesem Verzeichnis ausgeführt werden)
*[ ] npm install muss ausgeführt werden, damit die notwendigen Pakete installiert werden.
*[ ] Als Einstiegspunkt im Container braucht es den Befehl: node server.js 

## Inhalt des originalen ReadMe Files

##### build Docker image called node-app. Execute from root

    docker build -t node-app .
    
##### push image to repo 

    docker tag node-app demo-app:node-1.0
